import React from "react";

import Note from "./Note"

const Body = (props) => {
  return (
    <div>
      {props.text}{props.feeling}
      <div>
        <Note text="Vou continuar praticando!" feeling={props.feeling}/>
      </div>
    </div>
  );
};

export default Body;

import React from "react";

const FooterNote = (props) => {
  return <div>{props.text}</div>;
};

export default FooterNote
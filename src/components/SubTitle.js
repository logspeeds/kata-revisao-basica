import React from "react";

import SubTitleNote from "./SubTitleNote";
const SubTitle = (props) => {
  return (
    <h2>
      {props.text}{props.feeling}
      <div>
        <SubTitleNote text="Manda mais repetição!" feeling={props.feeling}/>
      </div>
    </h2>
  );
};

export default SubTitle;

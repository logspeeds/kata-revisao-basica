import React from "react";

import FooterNote from "./FooterNote";

const Footer = (props) => {
  return (
    <div>
      {props.text}{props.feeling}
      <div>
        <FooterNote text="Repetir mais!" feeling={props.feeling}/>
      </div>
    </div>
  );
};

export default Footer;

import React from "react";

import TitleNote from "./TitleNote";

const Title = (props) => {
  return (
    <h1>
      {props.text}{props.feeling}
      <div>
        <TitleNote text="Repetir mais ainda!" feeling={props.feeling}/>
      </div>
    </h1>
  );
};

export default Title;

Kata - Revisão básica

 

Para fortalecer nossos conhecimentos, nada melhor do que praticá-los! Através da repetição com uma pitada de progressão na complexidade nós ficamos com maior familiaridade no conteúdo! Vamos botão esse hábito em prática? :D

Nesse Kata, vamos botar em prática o uso de Props.

As props são usadas para transitar valores entre componentes, sempre do nível mais alto para o mais baixo! 

Props podem ser considerada como parâmetros de qualquer outra função:

 function MyComponent(props) {

      Return <h1>{props.title} </h1>
}

 

Caso a chamada da função MyComponente fosse como qualquer outra, ela seria da seguinte maneira:

MyComponent({title: "Aprenda React");

 

Mas no React Way, fazemos a mesma chamada, porém em uma estética diferente usando o JSX:

<MyComponent  title="Aprenda React"/>

 

Simples, né?

 

Agora vamos botar isso em prática! Siga os seguintes passo:

 

Crie um projeto pelo CRA chamado kata1
Instale as dependências do projeto
Inicie o projeto
No diretório src/ crie um novo diretório chamado components
 

Agora nós temos um diretório para os componentes e nosso projeto está pronto para botarmos a mão na massa!

 

Vamos começar criando nosso primeiro componente:

No diretório src/components crie um arquivo chamado Title.js
No novo arquivo, importe o React
import React from 'react';

 

Crie uma função com o mesmo nome do arquivo
const Title = () => {}

 

Exporte essa function para que seja possível importá-la no futuro. Coloque o seguinte comando na última linha do arquivo
 

Export default Title;

 

Faça esse componente retornar uma tag de título
           Const Title = () => {

                Return (

                    <h1></h1>

                )

            }

 

Como já sabemos, esse componente vai ser chamado recebendo um objeto do JS recebendo todas as props, então nomeie o primeiro parâmetro com props.
 

           const Title = (props) => {

                Return (

                    <h1></h1>

                )

            }

 

Vamos considerar que iremos receber uma props chamada text. Como já sabemos, o componente Title, no fundo é uma função e ele receberá todas as props em um único objeto por parâmetro. Nesse caso, podemos contar que recebemos a prop text dessa forma
    { text: "Meu título" }

 

Então, conseguimos usar dessa forma;

    props.text

 

Então vamos colocá-la dentro do h1. Para o conteúdo dentro das tags em JSX ser considerado um comando do JS, precisamos colocar-lo entre {}. 
         const Title = (props) => {

                Return (

                    <h1>{props.text}</h1>

                )

            }

 

Pronto, temos nosso componente que com a funcionalidade de receber um título por prop e delegar-lo para dentro do h1




Agora, dentro do arquivo src/App.js, no return do método App, remova o excesso de conteúdo. Deixe o arquivo dessa forma:

    import React from 'react';

    import './App.css';

    const App = () => {

          return(

   <div>

    </div>

         )

   } 

 

    

     

Agora, dentro do arquivo src/App.js, importe o componente Title nas primeiras linhas, logo abaixo do import do App.css:

 

Import Title from './components/Title';

 

Agora que temos o componente importado, vamos chama-lo dentro da div retornada pelo App

     const App = () => {

          return(

   <div>

       <Title />

    </div>

         )

   } 

 

Mas agora precisamos passar a prop texto, certo? Vamos passar a string "Aprendendo React". Basta adicionar seguir o padrão do html de passar propriedades ou atributos para uma tag:

 <Title text="Aprendendo React"/>

 

E o resultado final do src/App.js  será:

    import React from 'react';

    import './App.css';

 

    const App = () => {

          return(

   <div>

     <Title text="Aprendendo React"/>

 

    </div>

         )

   } 

 

Beleza, agora que já temos a base do nosso Kata, vamos brincar um pouco!

 

Seguindo a lógica e o passos que usamos para criar nosso componente Title, crie os seguintes componentes:

 

Componente SubTitle, que retorna um h2 e renderiza em seu conteúdo a prop Text. Chame esse componente SubTitle, logo abaixo do Title, com a string "Kata sobre props.";
Componente Body, que retorna uma Div e renderiza em seu conteúdo a prop Text. Chame esse componente Body, logo abaixo do SubTitle, com a string "Prática leva a perfeição!"
Componente Footer, que retorna uma Div e renderiza em seu conteúdo a prop  Text. Chame esse componente Footer, logo abaixo do Body, com a string "Repetição para praticar!"
 

Agora, lá vem um pequeno aumento na complexidade!

 

Seguindo o mesmo caminho, crie um componente chamado Note, que retorna uma Div e renderiza em seu conteúdo a prop Text com a string "Vou continuar praticando!". Mas esse componente deve ser importado dentro do componente Body e chamado dentro de outra div logo após da prop text. Dessa forma:

const Body = (props) => {

                Return (

                    <div>

                       {props.text}

                       <div>

                         <Note text= "Vou continuar praticando!" />

                       </div>

                  </div>

                )

            }

 

Agora seguindo a mesma linha de raciocínio, vamos criar mais componentes dentro de componentes!

Componente FooterNote, que retorna uma Div e renderiza em seu conteúdo a prop  Text. Chame esse componente FooterNote, dentro do componente Footer, dentro de uma div, logo após ao  {props.text} com o text "Repetir mais!"
 

Componente TitleNote, que retorna uma Div e renderiza em seu conteúdo a prop  Text. Chame esse componente TitleNote, dentro do componente Title, dentro de uma div, logo após ao  {props.text} com o text "Repetir mais ainda!"
 

Componente SubTitleNote, que retorna uma Div e renderiza em seu conteúdo a prop  Text. Chame esse componente SubTitleNote, dentro do componente Title, dentro de uma div, logo após ao  {props.text} com o text "Manda mais repetição!"
 

Nossa… quanto componente! Mas vamos praticar só mais uma coisinha! Que tal passar um prop nova lá do componente App, para o conteúdo dessa prop renderizado no componente conteúdo do Note? Ou seja, a prop desceria 2 níveis! Quase o roteiro do filme Inception!

 

Vamos lá!

Volte para o arquivo src/App. Na chamada do componente Body, passe mais uma prop chamada feeling coma string "<3". Dessa forma:

<Body text="Prática leva a perfeição!" feeling="<3"/>

 

Agora quero que dentro do componente Body, você pegue essa prop nova (props.feeling) e passe, novamente, por prop na chamada do componente Note. Dessa forma:

<Note text="Prática leva a perfeição!" feeling={props.feeling} />

 

E dentro do Note, você renderize o conteúdo do props.feeling logo depois do lugar que está renderizando o conteúdo do props.text. Vai ficar mais ou menos assim: {props.text} {props.feeling}

 

Agora, como não poderia deixar de faltar em um Kata, vamos repetir e repetir! Agora faça com que FooterNote, TitleNote e SuTitleNote também recebam a prop feeling com o conteúdo "<3"  sendo delegada lá do componente App.

 

Sei que você consegue fazer! :D

 

E por ora é só!